package Task2;

import java.util.UUID;

public class Methods {

    public String one(int number){
        return Integer.toString(number);
    }

    protected int two(int twoNumber){
        twoNumber = twoNumber + 100;
        return  twoNumber;
    }

    private String free(String freeNumber){
        freeNumber=freeNumber + UUID.randomUUID();
        return freeNumber;
    }
}
