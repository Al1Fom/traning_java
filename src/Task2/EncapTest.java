package Task2;

public class EncapTest {
    int number;
    int twoNumber;
    String freeNumber;

    public int getNumber() {
        return number;
    }//точка доступа к параметру number - получение параметра

    public void setNumber(int number) {
        this.number = number;
    }//точка доступа к параметру number - установка значения параметра

    public int getTwoNumber() {
        return twoNumber;
    }//точка доступа к параметру twoNumber - получение параметра

    public void setTwoNumber(int twoNumber) {
        this.twoNumber = twoNumber;
    }//точка доступа к параметру twoNumber - установка значения параметра

    public String getFreeNumber() {
        return freeNumber;
    }//точка доступа к параметру freeNumber - получение параметра

    public void setFreeNumber(String freeNumber) {
        this.freeNumber = freeNumber;
    }//точка доступа к параметру freeNumber - установка значения параметра

}
